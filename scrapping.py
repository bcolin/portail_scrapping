from urllib.request import *
from urllib.error import URLError
import socket



liste = open("tadam.txt").read().splitlines()

for name in liste:
    url = "http://www.eleves.mines-paris.eu/static/" + name
    print("requesting url : " + url)
    
    #attempting to download the picture     
    while(True):
        try:
            urlretrieve(url, './pics/' + name) # if this line throws an error, the 3 lines below won't be
                                           # executed and we'll move on to the 'except' section

            pic_download_successful = True  # we've reached this line : it means the image has been downloaded
            break                           # since the download was succesful, we can get out of the while loop
        except (socket.timeout, URLError, ConnectionResetError):
            if (j<3):
                #if the request times out, we try it again up to 3 times
                j = j+1
                print('timeout error. ' + name + ' download attempt n°' + str(j) )
            else:
                pic_download_successful = False    # we've tryed 3 times and failed 3 times ...
                break                              # we're giving up and getting out of the loop :(

    if (pic_download_successful):
        print('pic ' + name + ' downloaded')
    else:
       print('timeout error. ' + name + ' download aborted.')
