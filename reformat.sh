#!/bin/bash

cd pics/
#mkdir -p icons

for file in `ls *.jpg`
do
	output="${file/.jpg/_icon.jpg}"
	output="./icons/$output"
	output128="${output/.jpg/128.jpg}" 

	ffmpeg -y -i $file -filter:v "crop=164:164:0:30" $output
	ffmpeg -y -i $output -vf scale=128:128 $output128
	rm $output
done
